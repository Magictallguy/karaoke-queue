<?php

if (!defined('SITE_ENABLE')) {
    define('SITE_ENABLE', true);
}
require_once __DIR__.DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'config.php';
if ('86.13.131.239' == $_SERVER['HTTP_CF_CONNECTING_IP']) {
    $composer = ROOT_PATH.'vendor'.DIRECTORY_SEPARATOR.'autoload.php';
    if (!file_exists($composer)) {
        exit('Composer required');
    }
    require_once $composer;
    $whoops = new \Whoops\Run();
    $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler());
    $whoops->register();
}
require_once LIB_PATH.'web-router.php';

require_once LIB_PATH.'pdo.class.php';
require_once LIB_PATH.'func.class.php';
$func = functions::getInstance();
$set = $func->getSettings();
spl_autoload_register(function ($class) {
    global $func;
    $fullPath = HANDLER_PATH.$class.'.php';
    if (file_exists($fullPath)) {
        $func->req_file($fullPath);
    }
});
class Index
{
    public static $startTime = null;
    public static $endTime = null;
    public static $served = [];

    public function __construct()
    {
        global $db, $func, $set, $my, $session;
        self::$startTime = microtime(true);
        $my = $func->getUser();
        if (null !== $my) {
            $db->query('UPDATE users SET time_last_action = NOW() WHERE id = ?');
            $db->execute([$my['id']]);
        }
        self::$served = [
            '/admin' => 'adminHandler',
            '/admin/:alpha' => 'adminHandler',
            '/admin/:alpha/:alpha' => 'adminHandler',
            '/admin/:alpha/:alpha/:number' => 'adminHandler',
            '/auth/:alpha' => 'authHandler',
            '/errors/:alpha' => 'errorsHandler',
            '/history' => 'historyHandler',
            '/history/:alpha' => 'historyHandler',
            '/history/:alpha/:number' => 'historyHandler',
            '/queue' => 'queueHandler',
            '/queue/:alpha' => 'queueHandler',
            '/queue/:alpha/:number' => 'queueHandler',
            '/top-requesters' => 'topHandler',
            '/:alpha' => 'indexHandler',
            '/' => 'queueHandler',
        ];
        $func->req_file(LIB_PATH.'page.class.php');
        ToroHook::add('404', function () {
            global $func;
            http_response_code(404);
            $func->req_file(VIEW_PATH.'error-documents/404.php');

            return;
        });
        header('X-Frame-Options: DENY');
        ob_start('ob_gzhandler');
        $func->req_file(LIB_PATH.'header.php');
        Toro::serve(self::$served);
        $func->req_file(LIB_PATH.'footer.php');
        $buffer = ob_get_contents();
        ob_end_clean();
        self::$endTime = microtime(true);
        $loadTime = self::$endTime - self::$startTime;
        $queueCnt = self::getQueueCount();
        $findRepl = [
            '{{QUEUE_COUNT}}' => $queueCnt,
            '{{QUEUE_COUNT_TITLE}}' => $queueCnt > 0 ? ' :: '.$func->format($queueCnt).' in queue' : '',
            '{{PAGE_LOAD}}' => number_format(self::$endTime - self::$startTime, 3),
        ];
        foreach ($findRepl as $find => $repl) {
            $buffer = str_replace($find, $repl, $buffer);
        }
        echo $buffer;
    }

    public static function isLoggedIn(): bool
    {
        global $db, $session;
        $id = $session->get('userid');
        if (ctype_digit($id)) {
            if ($db->exists('users', 'id', $id)) {
                return true;
            }
        }

        return false;
    }

    public static function getQueueCount(): int
    {
        global $db;
        $db->query('SELECT COUNT(id) FROM track_queue WHERE played = 0 AND cleared = 0');
        $db->execute();
        $cnt = $db->result();

        return $cnt > 0 ? $cnt : 0;
    }

    public static function sessionMessageHandling(): ?string
    {
        global $session;
        $out = '';
        $keys = ['error', 'warning', 'info', 'success'];
        foreach ($keys as $key) {
            ${$var} = $session->get($key);
            if (null !== ${$var}) {
                $out .= '
                    <div class="alert alert-'.('error' != $key ? $key : 'danger').'">
                        <strong>'.ucfirst($key).'</strong><br>
                        '.${$var}.'
                    </div>
                ';
                $session->put($key, null);
            }
        }

        return '' !== $out ? $out : null;
    }
}
$app = new Index();
