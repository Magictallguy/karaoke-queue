<?php

class SecureSessionHandler extends SessionHandler
{
    protected $key;
    protected $name;
    protected $cookie;
    protected $domainName;
    protected $host;

    public function __construct($key, $name = 'YM_SESH_', $cookie = [])
    {
        $this->host = $_SERVER['HTTP_HOST'];
        $this->key = substr(hash('sha256', $key), 0, 32);
        $this->name = $name;
        $this->cookie = $cookie;
        $this->domainName = '.'.$this->host;
        $this->cookie += [
            'lifetime' => 0,
            'path' => '/',
            'domain' => $this->domainName,
            'secure' => isset($_SERVER['HTTPS']),
            'httponly' => true,
        ];
        $this->setup();
    }

    private function setup()
    {
        ini_set('session.use_cookies', 1);
        ini_set('session.use_only_cookies', 1);
        ini_set('session.save_handler', 'files');
        session_name($this->name);
        session_set_cookie_params($this->cookie['lifetime'], $this->cookie['path'], $this->cookie['domain'], $this->cookie['secure'], $this->cookie['httponly']);
    }

    public function start()
    {
        if ('' === session_id()) {
            if (session_start()) {
                return true; // return 0 === mt_rand(0, 4) ? $this->refresh() : true; // 1/5
            }
        }

        return false;
    }

    public function forget()
    {
        if ('' === session_id()) {
            return false;
        }
        $_SESSION = [];
        setcookie($this->name, '', time() - 42000, $this->cookie['path'], $this->cookie['domain'], $this->cookie['secure'], $this->cookie['httponly']);

        return session_destroy();
    }

    public function refresh()
    {
        return true; // session_regenerate_id(true);
    }

    public function read($id)
    {
        return (string) openssl_decrypt(parent::read($id), 'aes-256-ecb', $this->key);
    }

    public function write($id, $data)
    {
        return parent::write($id, openssl_encrypt($data, 'aes-256-ecb', $this->key));
    }

    public function isExpired($ttl = 30)
    {
        $last = isset($_SESSION['_last_activity']) ? $_SESSION['_last_activity'] : false;
        if (false !== $last && time() - $last > $ttl * 60) {
            return true;
        }
        $_SESSION['_last_activity'] = time();

        return false;
    }

    public function isFingerprint()
    {
        $hash = md5($_SERVER['HTTP_USER_AGENT'].(ip2long($_SERVER['REMOTE_ADDR']) & ip2long('255.255.0.0')));
        if (isset($_SESSION['_fingerprint'])) {
            return $_SESSION['_fingerprint'] === $hash;
        }
        $_SESSION['_fingerprint'] = $hash;

        return true;
    }

    public function isValid($ttl = 0)
    {
        return !$this->isExpired($ttl) && $this->isFingerprint();
    }

    public function get($name)
    {
        // prevent the session is started
        if ('' === session_id()) {
            $this->start();
        }
        $parsed = explode('.', $name);
        $result = $_SESSION;
        while ($parsed) {
            $next = array_shift($parsed);
            if (isset($result[$next])) {
                $result = $result[$next];
            } else {
                return null;
            }
        }

        return $result;
    }

    public function put($name, $value)
    {
        // prevent the session is started
        if ('' === session_id()) {
            $this->start();
        }
        $parsed = explode('.', $name);
        $session = &$_SESSION;
        while (count($parsed) > 1) {
            $next = array_shift($parsed);
            if (!isset($session[$next]) || !is_array($session[$next])) {
                $session[$next] = [];
            }
            $session = &$session[$next];
        }
        $session[array_shift($parsed)] = $value;
    }

    public function del($name)
    {
        $session = &$_SESSION;
        if (array_key_exists($name, $session)) {
            unset($_SESSION[$name]);
        }
    }

    public function getSessionID()
    {
        return session_id();
    }
}
$session = new SecureSessionHandler(SESSION_KEY);
session_set_save_handler($session, true);
// session_save_path(dirname(__DIR__).'/sessions');
$session->start();
