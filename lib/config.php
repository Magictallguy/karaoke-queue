<?php

if (!defined('SITE_ENABLE')) {
    exit;
}
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'example');

define('ROOT_PATH', dirname(__DIR__).DIRECTORY_SEPARATOR);
define('LIB_PATH', __DIR__.DIRECTORY_SEPARATOR);
define('HANDLER_PATH', ROOT_PATH.DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'handler'.DIRECTORY_SEPARATOR);
define('VIEW_PATH', ROOT_PATH.DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'view'.DIRECTORY_SEPARATOR);
define('SESSION_KEY', 'someRandomKey');

define('ADMIN_IDS', [1]);
global $session;

require_once LIB_PATH.'session.class.php';