<?php
if (!defined('SITE_ENABLE')) {
    exit;
}
define('PAGE_END', microtime(true)); ?>
        </div>
    </div>
    <!-- /#page-content-wrapper -->
</div>
<!-- /#wrapper -->
<footer class="footer bg-dark text-light text-center">
    <div class="container">
        <span class="small">Copyright &copy; 2019, Magictallguy/Orsokuma</span><br>
        <span class="small"><span style="font-size:0.8em;">Page loaded in: {{PAGE_LOAD}}</span></span>
    </div>
</footer>

<!-- Bootstrap core JavaScript -->
<script src="/vendor/jquery/jquery.min.js"></script>
<script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>
</html>
