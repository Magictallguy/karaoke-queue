<?php

if (!defined('SITE_ENABLE')) {
    exit;
}
class functions
{
    public static $inst = null;
    public $settingsCache = [];
    public $fileCache = [];

    public static function getInstance()
    {
        if (null == self::$inst) {
            self::$inst = new self();
        }

        return self::$inst;
    }

    public function ordinal($num)
    {
        $ends = ['th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th'];
        if ((($num % 100) >= 11) && (($num % 100) <= 13)) {
            return $num.'th';
        } else {
            return $num.$ends[$num % 10];
        }
    }

    public function format($str, $dec = 0)
    {
        if (is_numeric($str)) {
            return number_format($str, $dec);
        } else {
            $ret = stripslashes(htmlspecialchars($str));
            if (true === $dec) {
                $ret = nl2br($ret);
            }

            return $ret;
        }
    }

    public function getUser($id = null)
    {
        global $db, $session;
        if (null === $id) {
            $id = $session->get('userid');
        }
        if (null !== $id) {
            $db->query('SELECT id, username, role_id, time_added, time_last_action FROM users WHERE id = ?');
            $db->execute([$id]);
            $ret = $db->fetch(true);
            if (null !== $ret) {
                return $ret;
            }
        }

        return null;
    }

    public function humanize($num)
    {
        if (1 == $num) {
            return 'Once';
        } elseif (2 == $num) {
            return 'Twice';
        } else {
            return $this->format($num).' times';
        }
    }

    public function time_format($seconds, $mode = 'long', $ago = false, $display = 3)
    {
        if (!$seconds) {
            return $ago ? 'Now' : 'Never';
        }
        $names = [
            'long' => ['millenia', 'year', 'month', 'day', 'hour', 'minute', 'second'],
            'short' => ['mil', 'yr', 'mnth', 'day', 'hr', 'min', 'sec'],
        ];
        $seconds = floor($seconds);
        $minutes = intval($seconds / 60);
        $seconds -= $minutes * 60;
        $hours = intval($minutes / 60);
        $minutes -= $hours * 60;
        $days = intval($hours / 24);
        $hours -= $days * 24;
        $months = intval($days / 31);
        $days -= $months * 31;
        $years = intval($months / 12);
        $months -= $years * 12;
        $millenia = intval($years / 1000);
        $years -= $millenia * 1000;
        $disp = 0;
        $result = [];
        if ($millenia && $disp < $display) {
            $result[] = sprintf('%s %s', number_format($millenia), $names[$mode][0]);
            ++$disp;
        }
        if ($years && $disp < $display) {
            $result[] = sprintf('%s %s%s', number_format($years), $names[$mode][1], $this->s($years));
            ++$disp;
        }
        if ($months && $disp < $display) {
            $result[] = sprintf('%s %s%s', number_format($months), $names[$mode][2], $this->s($months));
            ++$disp;
        }
        if ($days && $disp < $display) {
            $result[] = sprintf('%s %s%s', number_format($days), $names[$mode][3], $this->s($days));
            ++$disp;
        }
        if ($hours && $disp < $display) {
            $result[] = sprintf('%s %s%s', number_format($hours), $names[$mode][4], $this->s($hours));
            ++$disp;
        }
        if ($minutes && $disp < $display) {
            $result[] = sprintf('%s %s%s', number_format($minutes), $names[$mode][5], $this->s($minutes));
            ++$disp;
        }
        if ($seconds && $disp < $display || !count($result)) {
            $result[] = sprintf('%s %s%s', number_format($seconds), $names[$mode][6], $this->s($seconds));
            ++$disp;
        }

        return implode(', ', $result).($ago ? ' ago' : '');
    }

    public function s($num)
    {
        return 1 == $num ? '' : 's';
    }

    public function username($id)
    {
        global $db;
        $db->query('SELECT id, username FROM users WHERE id = ?');
        $db->execute([$id]);

        return $db->result(1);
    }

    public function isProxy()
    {
        $httpHeaders = [
            'HTTP_VIA',
            // 'HTTP_X_FORWARDED_FOR', // Set by CloudFlare. Comment out to disable check
            'HTTP_FORWARDED_FOR',
            'HTTP_X_FORWARDED',
            'HTTP_FORWARDED',
            'HTTP_CLIENT_IP',
            'HTTP_FORWARDED_FOR_IP',
            'VIA',
            'X_FORWARDED_FOR',
            'FORWARDED_FOR',
            'X_FORWARDED',
            'FORWARDED',
            'CLIENT_IP',
            'FORWARDED_FOR_IP',
            'HTTP_PROXY_CONNECTION',
        ];
        foreach ($httpHeaders as $header) {
            if (array_key_exists($header, $_SERVER) && isset($_SERVER[$header]) && strlen($_SERVER[$header]) > 0) {
                return true;
            }
        }

        return false;
    }

    /*
     * Function: _ip
     * Capture user's given IP address (not necessarily accurate)
     * Params: none
     */
    public function _ip()
    {
        $check = $this->isCloudflare();

        if ($check) {
            return $_SERVER['HTTP_CF_CONNECTING_IP'];
        } else {
            $types = [
                'HTTP_CLIENT_IP',
                'HTTP_X_FORWARDED_FOR',
                'HTTP_X_FORWARDED',
                'HTTP_X_CLUSTER_CLIENT_IP',
                'HTTP_FORWARDED_FOR',
                'HTTP_FORWARDED',
                'REMOTE_ADDR',
            ];
            foreach ($types as $key) {
                if (true === array_key_exists($key, $_SERVER)) {
                    foreach (explode(',', $_SERVER[$key]) as $ip) {
                        $ip = trim($ip);
                        if (false !== filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE)) {
                            return $ip;
                        }
                    }
                }
            }
        }

        return '127.0.0.1';
    }

    public function subnetMatch($client_ip = false, $server_ip = false)
    {
        if (!$client_ip) {
            $client_ip = $this->_ip();
        }
        if (!$server_ip) {
            $server_ip = $_SERVER['SERVER_ADDR'];
        }

        // Extract broadcast and netmask from ifconfig
        if (!($p = popen('ifconfig', 'r'))) {
            return false;
        }
        $out = '';
        while (!feof($p)) {
            $out .= fread($p, 1024);
        }
        fclose($p);

        // This is to avoid wrapping.
        $match = '/^.*'.$server_ip;
        $match .= ".*Bcast:(\d{1,3}\.\d{1,3}i\.\d{1,3}\.\d{1,3}).*";
        $match .= "Mask:(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})$/im";
        if (!preg_match($match, $out, $regs)) {
            return false;
        }

        $bcast = ip2long($regs[1]);
        $smask = ip2long($regs[2]);
        $ipadr = ip2long($client_ip);
        $nmask = $bcast & $smask;

        return ($ipadr & $smask) == ($nmask & $smask);
    }

    // CLOUDFLARE
    public function ip_in_range($ip, $range)
    {
        if (false == strpos($range, '/')) {
            $range .= '/32';
        }

        // $range is in IP/CIDR format eg 127.0.0.1/24
        list($range, $netmask) = explode('/', $range, 2);
        $range_decimal = ip2long($range);
        $ip_decimal = ip2long($ip);
        $wildcard_decimal = pow(2, (32 - $netmask)) - 1;
        $netmask_decimal = ~$wildcard_decimal;

        return ($ip_decimal & $netmask_decimal) == ($range_decimal & $netmask_decimal);
    }

    public function _cloudflare_CheckIP($ip)
    {
        $cf_ips = [
            '199.27.128.0/21',
            '173.245.48.0/20',
            '103.21.244.0/22',
            '103.22.200.0/22',
            '103.31.4.0/22',
            '141.101.64.0/18',
            '108.162.192.0/18',
            '190.93.240.0/20',
            '188.114.96.0/20',
            '197.234.240.0/22',
            '198.41.128.0/17',
            '162.158.0.0/15',
            '104.16.0.0/12',
        ];
        $is_cf_ip = false;
        foreach ($cf_ips as $cf_ip) {
            if ($this->ip_in_range($ip, $cf_ip)) {
                $is_cf_ip = true;
                break;
            }
        }

        return $is_cf_ip;
    }

    public function _cloudflare_Requests_Check()
    {
        $flag = true;

        if (!isset($_SERVER['HTTP_CF_CONNECTING_IP'])) {
            $flag = false;
        }
        if (!isset($_SERVER['HTTP_CF_IPCOUNTRY'])) {
            $flag = false;
        }
        if (!isset($_SERVER['HTTP_CF_RAY'])) {
            $flag = false;
        }
        if (!isset($_SERVER['HTTP_CF_VISITOR'])) {
            $flag = false;
        }

        return $flag;
    }

    public function isCloudflare()
    {
        $ipCheck = $this->_cloudflare_CheckIP($_SERVER['REMOTE_ADDR']);
        $requestCheck = $this->_cloudflare_Requests_Check();

        return $ipCheck && $requestCheck;
    }

    public function autoVersion($file)
    {
        $filetime = 0 === strpos($file, '/') ? (file_exists($_SERVER['DOCUMENT_ROOT'].$file) ? filemtime($_SERVER['DOCUMENT_ROOT'].$file) : false) : false;
        if ($filetime) {
            return preg_replace('{\\.([^./]+)$}', ".$filetime.\$1", $file);
        } else {
            return $file;
        }
    }

    public function getSettings()
    {
        global $db;
        if (count($this->settingsCache) > 0) {
            return $this->settingsCache;
        }
        $db->query('SELECT SQL_CACHE id, conf_name, conf_value FROM settings ORDER BY id ASC');
        $db->execute();
        $settings = $db->fetch();
        if (null !== $settings) {
            foreach ($settings as $row) {
                $this->settingsCache[$row['conf_name']] = $row['conf_value'];
            }
        }

        return $this->settingsCache;
    }

    public function req_file(string $path)
    {
        $path = str_replace(['\\', '\\\\', '/'], DIRECTORY_SEPARATOR, $path);
        if (file_exists($path)) {
            $isIncluded = false;
            $isIncludedFile = '';
            if (!count($this->fileCache)) {
                $this->fileCache = get_included_files();
            }
            foreach ($this->fileCache as $file) {
                if ($file == $path) {
                    $isIncluded = true;
                    $isIncludedFile = $file;
                }
            }
            if (false === $isIncluded) {
                require_once $path;
            }
        } else {
            throw new Exception('File missing: '.$path);
        }
    }

    public function perm($what, $id = 0): bool
    {
        global $db, $session;
        if (!$id) {
            $id = $session->get('userid');
            if (!$id) {
                return false;
            }
        }
        if (!isset($permCache)) {
            $permCache = [];
        }
        if (is_array($what) && count($what) > 0) {
            foreach ($what as $wut) {
                if (array_key_exists($wut.$id, $permCache)) {
                    return $permCache[$wut.$id];
                }
            }
            $db->query('SELECT SQL_CACHE role_id FROM users WHERE id = ?');
            $db->execute([$id]);
            $rank = $db->result();
            foreach ($what as $wut) {
                if (!$rank && !array_key_exists($wut.$id, $permCache)) {
                    $permCache[$wut.$id] = false;
                }
            }
            $db->query('SELECT SQL_CACHE id, '.implode(', ', $what).', override_all FROM roles WHERE id = ?');
            $db->execute([$rank]);
            $perm = $db->fetch(true);
            if (false === $perm) {
                foreach ($what as $wut) {
                    if (!array_key_exists($wut.$id, $permCache)) {
                        $permCache[$wut.$id] = false;
                    }
                }
            }
            if (1 == $perm['override_all']) {
                foreach ($what as $wut) {
                    if (!array_key_exists($wut.$id, $permCache)) {
                        $permCache[$wut.$id] = true;
                    }
                }

                return true;
            } else {
                foreach ($what as $wut) {
                    if (1 != $perm[$wut]) {
                        if (!array_key_exists($wut.$id, $permCache)) {
                            $permCache[$wut.$id] = false;
                        }
                    }
                    if (!array_key_exists($wut.$id, $permCache)) {
                        $permCache[$wut.$id] = true;
                    }
                }
            }

            return in_array(true, $permCache, true);
        } else {
            if (array_key_exists($what.$id, $permCache)) {
                return $permCache[$what.$id];
            }
            $db->query('SELECT SQL_CACHE role_id FROM users WHERE id = ?');
            $db->execute([$id]);
            $rank = $db->result();
            if (false === $rank) {
                if (!array_key_exists($what.$id, $permCache)) {
                    $permCache[$what.$id] = false;
                }

                return false;
            }
            if (!$rank) {
                if (!array_key_exists($what.$id, $permCache)) {
                    $permCache[$what.$id] = false;
                }

                return false;
            }
            $db->query('SELECT SQL_CACHE '.$what.', override_all FROM roles WHERE id = ?');
            $db->execute([$rank]);
            $perm = $db->fetch(true);
            if (false === $perm) {
                if (!array_key_exists($what.$id, $permCache)) {
                    $permCache[$what.$id] = false;
                }

                return false;
            }
            if (1 == $perm['override_all']) {
                if (!array_key_exists($what.$id, $permCache)) {
                    $permCache[$what.$id] = false;
                }

                return true;
            }
            if (1 != $perm[$what]) {
                if (!array_key_exists($what.$id, $permCache)) {
                    $permCache[$what.$id] = false;
                }

                return false;
            }
            if (!array_key_exists($what.$id, $permCache)) {
                $permCache[$what.$id] = true;
            }

            return true;
        }

        return false;
    }

    public function alert($msg, $type)
    {
        $head = 'danger' != $type ? $type : 'error'; ?>
        <div class="alert alert-<?php echo $type; ?>">
            <strong><?php echo ucfirst($head); ?></strong><br>
            <?php echo $msg; ?>
        </div><?php
    }

    public function getSinger($str): array
    {
        if (in_array($str, ['All', ''])) {
            $singers = null;
            $singerCnt = null;
        } else {
            $singers = explode(',', $str);
            $singerCnt = count($singers);
            if (!$singerCnt) {
                $singers = null;
            }
        }
        if (1 == $singerCnt) {
            $singer = ($row['is_solo'] ? 'Solo: ' : 'Group song. Lead: ').$this->format($str);
        } elseif (2 == $singerCnt) {
            $singer = ($row['is_solo'] ? 'Duet: ' : 'Group song. Leads: ').$this->format($singers[0]).' and '.$this->format($singers[1]);
        } elseif (3 == $singerCnt) {
            $singer = ($row['is_solo'] ? 'Trio: ' : 'Group song. Leads: ').$this->format($singers[0]).', '.$this->format($singers[1]).' and '.$this->format($singers[2]);
        } else {
            $singer = 'Group song';
        }

        return [
            'singer' => $singer,
            'singerCnt' => $singerCnt,
            'singersArray' => $singers,
        ];
    }
}
$func = functions::getInstance();
