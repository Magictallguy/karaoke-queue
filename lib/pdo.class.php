<?php
/*
 * DON'T BE A DICK PUBLIC LICENSE
 * Everyone is permitted to copy and distribute verbatim or modified copies of this license document, and changing it is allowed as long as the name is changed.
 *
 * DON'T BE A DICK PUBLIC LICENSE TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 * Do whatever you like with the original work, just don't be a dick.
 *
 * Being a dick includes - but is not limited to - the following instances:
 *
 * 1a. Outright copyright infringement - Don't just copy this and change the name.
 * 1b. Selling the unmodified original with no work done what-so-ever, that's REALLY being a dick.
 * 1c. Modifying the original work to contain hidden harmful content. That would make you a PROPER dick.
 *
 * If you become rich through modifications, related works/services, or supporting the original work, share the love. Only a dick would make loads off this work and not buy the original works creator(s) a pint.
 *
 * Code is provided with no warranty. Using somebody else's code and bitching when it goes wrong makes you a DONKEY dick. Fix the problem yourself. A non-dick would submit the fix back.
 */
if (!defined('SITE_ENABLE')) {
    exit;
}
class pdo_db extends PDO
{
    protected $last_query;
    protected $execution;
    protected $rowsFromFetch;
    protected $resultFromFetch;
    protected $resultFromObject;
    protected $activeTransaction = false;
    protected $conn;
    protected $rowCount = 0;
    protected $transactionCounter = 0;
    private static $host = DB_HOST;
    private static $user = DB_USER;
    private static $pass = DB_PASS;
    // humps for days
    private static $name = DB_NAME;
    private $db;
    private $stmt;
    public static $inst = null;
    public $binds = [];
    public $queryCount = 0;

    public static function getInstance()
    {
        if (null == self::$inst) {
            self::$inst = new self();
        }

        return self::$inst;
    }

    public function __construct()
    {
        mb_internal_encoding('UTF-8');
        mb_regex_encoding('UTF-8');
        try {
            $opts = [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_PERSISTENT => true,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_EMULATE_PREPARES => 0,
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8mb4;',
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET CHARACTER SET utf8mb4;',
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET SQL_BIG_SELECTS = 1;',
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET TRANSACTION ISOLATION LEVEL READ COMMITTED;',
            ];
            $this->db = new PDO('mysql:host='.static::$host.';dbname='.static::$name.';charset=utf8', static::$user, static::$pass, $opts);
        } catch (PDOException $e) {
            if (strpos($e->getMessage(), 'No such file or directory')) {
                exit();
            }
            throw new Exception('CONSTRUCT ERROR'."\n".$e->getMessage());
        }
    }

    public function __destruct()
    {
        if (!$this->db) {
            return null;
        }
        $this->db = null;

        return null;
    }

    /**
     * @param string $query
     */
    public function query($query)
    {
        $this->last_query = $query;
        ++$this->queryCount;
        try {
            $this->stmt = $this->db->prepare($query);
        } catch (PDOException $e) {
            throw new Exception('QUERY ERROR'."\n".$e->getMessage()."\nQuery was: ".$this->last_query);
        }
    }

    public function bind($param, $value, $type = null)
    {
        if (is_null($type)) {
            switch (true) {
                case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = PDO::PARAM_NULL;
                    break;
                default:
                    $type = PDO::PARAM_STR;
                    break;
            }
        } else {
            switch ($type) {
                case 'int':
                    $type = PDO::PARAM_INT;
                    break;
                case 'float':
                    $type = PDO::PARAM_FLOAT;
                    break;
                case 'str':
                case 'string':
                    $type = PDO::PARAM_STR;
                    break;
                case 'null':
                    $type = PDO::PARAM_NULL;
                    break;
                case 'bool':
                    $type = PDO::PARAM_BOOL;
                    break;
            }
        }
        try {
            $this->stmt->bindValue($param, $value, $type);
        } catch (PDOException $e) {
            error_log($e->getMessage());
            throw new Exception('BIND ERROR'."\n".$e->getMessage());
        }
    }

    public function execute(array $binds = null)
    {
        $this->binds = $binds;
        if (!isset($this->stmt)) {
            $this->execution = false;
        } else {
            try {
                $this->execution = is_array($binds) && count($binds) > 0 ? $this->stmt->execute($binds) : $this->stmt->execute();
            } catch (PDOException $e) {
                error_log($e->getMessage());
                throw new Exception('EXECUTION ERROR'."\n".$e->getMessage()."\nQuery was: ".$this->last_query);
                var_dump($this->stmt->debugDumpParams());
                echo '</pre></p>';
                exit();
            }
        }

        return $this->execution;
    }

    public function fetch($shift = false)
    {
        if (!isset($this->stmt)) {
            $this->rowsFromFetch = null;
        } else {
            try {
                $this->execute();
                $this->rowsFromFetch = $this->stmt->fetchAll(PDO::FETCH_ASSOC);
                if ($shift) {
                    $this->rowsFromFetch = array_shift($this->rowsFromFetch);
                }
            } catch (PDOException $e) {
                error_log($e->getMessage());
                throw new Exception('FETCH ROW ERROR'."\n".$e->getMessage());
            }
        }
        if (null === $this->rowsFromFetch || (is_array($this->rowsFromFetch) && !count($this->rowsFromFetch)) || !$this->rowsFromFetch) {
            return null;
        }
        $this->stmt->closeCursor();

        return $this->rowsFromFetch;
    }

    public function fetchKey($shift = false)
    {
        if (!isset($this->stmt)) {
            $this->rowsFromFetch = null;
        } else {
            try {
                $this->execute();
                $this->rowsFromFetch = $this->stmt->fetchAll(PDO::FETCH_BOTH);
                if ($shift) {
                    $this->rowsFromFetch = array_shift($this->rowsFromFetch);
                }
            } catch (PDOException $e) {
                error_log($e->getMessage());
                throw new Exception('FETCH ROW ERROR'."\n".$e->getMessage());
            }
        }
        if (null === $this->rowsFromFetch || (is_array($this->rowsFromFetch) && !count($this->rowsFromFetch)) || !$this->rowsFromFetch) {
            return null;
        }
        $this->stmt->closeCursor();

        return $this->rowsFromFetch;
    }

    public function result($col = 0, $asBool = false)
    {
        if (true === $col) {
            $asBool = true;
            $col = 0;
        }
        if (!isset($this->stmt)) {
            $this->resultFromFetch = null;
        } else {
            try {
                $this->execute();
                $this->resultFromFetch = $this->stmt->fetchColumn($col);
            } catch (PDOException $e) {
                error_log($e->getMessage());
                throw new Exception('FETCH SINGLE ERROR'."\n".$e->getMessage());
            }
        }
        $ret = $this->resultFromFetch ? $this->resultFromFetch : 0;
        $this->stmt->closeCursor();

        return true === $asBool ? (bool) $ret : $ret;
    }

    public function fetch_object()
    {
        if (!isset($this->stmt)) {
            $this->resultFromFetch = null;
        } else {
            try {
                $this->execute();
                $this->resultFromObject = $this->stmt->fetch(PDO::FETCH_OBJ);
            } catch (PDOException $e) {
                error_log($e->getMessage());
                throw new Exception('FETCH OBJECT ERROR'."\n".$e->getMessage());
            }
        }
        if (!count($this->resultFromFetch)) {
            return null;
        }
        $this->stmt->closeCursor();

        return $this->resultFromFetch;
    }

    public function affected()
    {
        try {
            return $this->stmt->rowCount();
        } catch (PDOException $e) {
            error_log($e->getMessage());
            throw new Exception('AFFECTED ROWS ERROR'."\n".$e->getMessage());
        }
    }

    public function count($asBool = false)
    {
        if (is_array($this->resultFromFetch)) {
            $this->rowCount = count($this->resultFromFetch);
        } else {
            try {
                $this->rowCount = $this->stmt->fetchColumn();
            } catch (PDOException $e) {
                error_log($e->getMessage());
                throw new Exception('NUM ROWS ERROR'."\n".$e->getMessage());
            }
        }
        $ret = $this->rowCount > 0 ? $this->rowCount : 0;
        $this->stmt->closeCursor();

        return true === $asBool ? (bool) $ret : $ret;
    }

    public function insert_id()
    {
        try {
            return $this->db->lastInsertId();
        } catch (PDOException $e) {
            error_log($e->getMessage());
            throw new Exception('LAST INSERT ID ERROR'."\n".$e->getMessage());
        }
    }

    public function colCnt()
    {
        try {
            return $this->stmt->columnCount();
        } catch (PDOException $e) {
            error_log($e->getMessage());
            throw new Exception('COLUMN COUNT ERROR'."\n".$e->getMessage());
        }
    }

    public function query_error()
    {
        global $session;
        $id = $session->get('userid');
        if (in_array($id, ADMIN_IDS)) {
            exit('<strong>QUERY ERROR:</strong> '.$this->error.'<br>Query was '.$this->last_query);
        } else {
            exit('An error has been detected');
        }
    }

    public function trans($action)
    {
        try {
            if ('start' == $action) {
                if (!$this->transactionCounter++) {
                    return $this->db->beginTransaction();
                }
                $this->db->exec('SAVEPOINT trans'.$this->transactionCounter);

                return $this->transactionCounter >= 0;
            } elseif ('end' == $action) {
                if (!--$this->transactionCounter) {
                    return $this->db->commit();
                }

                return $this->transactionCounter >= 0;
            } else {
                if (--$this->transactionCounter) {
                    $this->db->exec('ROLLBACK TO trans'.($this->transactionCounter + 1));

                    return true;
                }

                return $this->db->rollback();
            }
        } catch (PDOException $e) {
            error_log($e->getMessage());
            exit('<strong>'.strtoupper($action).' TRANSACTION ERROR:</strong> '.$e->getMessage());
        }
    }

    public function error()
    {
        echo '<pre class="white">';
        var_dump($this->stmt->debugDumpParams());
        echo '</pre>';
    }

    // Helper function(s)
    public function truncate(array $tables = null, $trans = false)
    {
        if (!count($tables)) {
            return false;
        }
        if (!$trans) {
            $this->trans('start');
        }
        foreach ($tables as $table) {
            $this->query('TRUNCATE TABLE '.$table.'');
            $this->execute();
        }
        if (!$trans) {
            $this->trans('end');
        }
    }

    public function tableExists($table)
    {
        try {
            $result = $this->db->query('SELECT 1 FROM '.$table.' LIMIT 1');
        } catch (Exception $e) {
            return false;
        }

        return false !== $result;
    }

    public function columnExists($column, $table)
    {
        try {
            $this->query('SHOW COLUMNS FROM '.$table.' WHERE Field = "'.$column.'"');
            $this->execute();
            $result = $this->fetch();
        } catch (Exception $e) {
            return false;
        }

        return false !== $result;
    }

    public function exists($table, $column, $id)
    {
        $this->query('SELECT COUNT('.$column.') FROM '.$table.' WHERE '.$column.' = ?');
        $this->execute([$id]);

        return (bool) $this->result();
    }
}
$db = pdo_db::getInstance();
