<?php
if (!defined('SITE_ENABLE')) {
    exit;
}
global $my, $set, $func;
$queueCnt = Index::getQueueCount(); ?><!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Bear's Karaoke Queue on Rabbit">
  <meta name="author" content="Magictallguy/Orsokuma">

  <title>Bear's Karaoke Queue{{QUEUE_COUNT_TITLE}}</title>

  <!-- Bootstrap core CSS -->
  <link href="<?php echo $func->autoVersion('/vendor/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo $func->autoVersion('/css/simple-sidebar.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo $func->autoVersion('/vendor/font-awesome/css/all.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo $func->autoVersion('/css/checkbox-toggle.min.css'); ?>" rel="stylesheet">

</head>

<body class="bg-dark text-light">

  <div class="d-flex bg-dark text-light" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-dark text-light border-right" id="sidebar-wrapper">
      <div class="list-group list-group-flush"><?php
      if (null !== $my) {
          ?>
            <a href="#" class="list-group-item list-group-item-action bg-dark text-light">Hi <?php echo $func->format($my['username']); ?></a><?php
        if ($func->perm('can_access_admin_panel')) {
            ?>
          <a href="/admin" class="list-group-item list-group-item-action bg-dark text-light">Admin Panel</a><?php
        }
      } else {
          ?>
            <a href="/auth/login" class="list-group-item list-group-item-action bg-dark text-light">Login</a><?php
            if ($set['enable_registration']) {
                ?>
              <a href="/auth/reg" class="list-group-item list-group-item-action bg-dark text-light">Register</a><?php
            }
      }?>
        <a href="/queue" class="list-group-item list-group-item-action bg-dark text-light">Karaoke Queue ({{QUEUE_COUNT}})</a>
        <a href="/history" class="list-group-item list-group-item-action bg-dark text-light">History</a>
        <a href="/top-requesters" class="list-group-item list-group-item-action bg-dark text-light">Top Requesters</a>
        <a href="/rule" class="list-group-item list-group-item-action bg-dark text-light">Rule</a>
        <a href="/FAQ" class="list-group-item list-group-item-action bg-dark text-light">FAQ</a><?php
        if (null !== $my) {
            ?>
            <a href="/auth/logout" class="list-group-item list-group-item-action bg-dark text-light">Logout</a><?php
        } ?>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
      <div class="container-fluid bg-dark text-light"><?php
        echo Index::sessionMessageHandling();
