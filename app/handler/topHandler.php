<?php

if (!defined('SITE_ENABLE')) {
    exit;
}
class topHandler
{
    public function get()
    {
        global $db, $func, $rows, $requesterCnt, $attributeClass;
        $attributeClass = null;
        $db->query('SELECT requested_by, COUNT(id) AS cnt
            FROM track_queue
            WHERE requested_by != ""
            GROUP BY requested_by
            ORDER BY cnt DESC, requested_by ASC
            LIMIT 25
        ');
        $db->execute();
        $rows = $db->fetch();
        $requesterCnt = count($rows);
        $attributeClass = [
            1 => 'text-success',
            2 => 'text-warning',
            3 => 'text-info',
        ];
        $func->req_file(VIEW_PATH.'top-requesters.php');

        return;
    }
}
