<?php

if (!defined('SITE_ENABLE')) {
    exit;
}
class indexHandler
{
    public function get($arg = null, $arg2 = null)
    {
        global $func;
        if ('FAQ' == $arg) {
            $func->req_file(VIEW_PATH.'faq.php');

            return;
        } elseif ('rule' == $arg) {
            $func->req_file(VIEW_PATH.'rule.php');

            return;
        } else {
            header('Location: /queue');

            return;
        }
    }
}
