<?php

if (!defined('SITE_ENABLE')) {
    exit;
}
class historyHandler
{
    public function get($arg = null, $arg2 = null)
    {
        global $db, $func, $my, $session;
        if ('restore' == $arg) {
            if (null !== $my && $func->perm('can_restore_to_queue')) {
                $arg2 = ctype_digit($arg2) && $arg2 > 0 ? $arg2 : null;
                if (null !== $arg2) {
                    $db->query('SELECT id, track, artist, played, cleared FROM track_queue WHERE id = ?');
                    $db->execute([$arg2]);
                    $row = $db->fetch(true);
                    if (null !== $row) {
                        if ($row['played']) {
                            if (!$row['cleared']) {
                                $db->query('UPDATE track_queue SET played = 0, cleared = 0 WHERE id = ?');
                                $db->execute([$arg2]);
                                $session->put('success', 'You\'ve restored '.$func->format($row['artist']).': '.$func->format($row['track']));
                            } else {
                                $session->put('error', $func->format($row['artist']).': '.$func->format($row['track']).' has been cleared from the history. It can\'t be restored');
                            }
                        } else {
                            $session->put('error', $func->format($row['artist']).': '.$func->format($row['track']).' has yet to be played');
                        }
                    } else {
                        $session->put('error', 'The listing you selected doesn\'t exist');
                    }
                } else {
                    $session->put('error', 'You didn\'t select a valid listing');
                }
            } else {
                $session->put('error', 'You don\'t have access');
            }
            header('Location: /history');

            return;
        } elseif ('clear' == $arg) {
            if (null !== $my && $func->perm('can_clear_history')) {
                $db->query('SELECT COUNT(id) FROM track_queue WHERE played = 1 AND cleared = 0');
                $db->execute();
                if ($db->result() > 0) {
                    $db->query('UPDATE track_queue SET cleared = 1 WHERE played = 1 AND cleared = 0');
                    $db->execute();
                    $aff = $db->affected();
                    $session->put('success', $func->format($aff).' track'.(1 == $aff ? ' has' : 's have').' been cleared');
                } else {
                    $session->put('error', 'There are no tracks to clear');
                }
            } else {
                $session->put('error', 'You don\'t have access');
            }
            header('Location: /history');

            return;
        } else {
            global $pages, $played, $history, $canRestore, $replayed;
            $replayed = 0;
            $canRestore = $func->perm('can_restore_to_queue');
            $played = [];
            $db->query('SELECT COUNT(id) AS cnt, calculated_hash FROM track_queue WHERE cleared = 0 GROUP BY calculated_hash');
            $db->execute();
            $stats = $db->fetch();
            if (null !== $stats) {
                foreach ($stats as $stat) {
                    $played[$stat['calculated_hash']] = $stat['cnt'];
                    if ($stat['cnt'] > 1) {
                        ++$replayed;
                    }
                }
            }
            $db->query('SELECT COUNT(id) FROM track_queue WHERE played = 1 AND cleared = 0');
            $db->execute();
            $cnt = $db->result();
            $pages = new Paginator($cnt, 'history');
            $db->query('SELECT * FROM track_queue WHERE played = 1 AND cleared = 0 ORDER BY time_added DESC'.$pages->limit);
            $db->execute();
            $history = $db->fetch();
            $func->req_file(VIEW_PATH.'history.php');

            return;
        }
    }
}
