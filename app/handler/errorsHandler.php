<?php

if (!defined('SITE_ENABLE')) {
    exit;
}

class errorsHandler
{
    public function get($arg = null)
    {
        if (in_array($arg, [400, 401, 403, 404, 500])) {
            $func->req_file(VIEW_PATH.$arg.'.php');

            return;
        } else {
            $func->req_file(VIEW_PATH.'index.php');

            return;
        }
    }
}
