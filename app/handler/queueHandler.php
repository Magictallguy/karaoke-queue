<?php

if (!defined('SITE_ENABLE')) {
    exit;
}
class queueHandler
{
    public function get($arg = null, $arg2 = null)
    {
        global $db, $func, $my, $session;
        $my = $func->getUser();
        if ('viewed' == $arg) {
            if ($func->perm('can_mark_queue')) {
                $arg2 = ctype_digit($arg2) && $arg2 > 0 ? $arg2 : null;
                if (null !== $arg2) {
                    $db->query('SELECT id, track, artist, played, cleared FROM track_queue WHERE id = ?');
                    $db->execute([$arg2]);
                    $row = $db->fetch(true);
                    if (null !== $row) {
                        if (!$row['cleared']) {
                            if (!$row['played']) {
                                $db->query('UPDATE track_queue SET played = 1 WHERE id = ?');
                                $db->execute([$arg2]);
                                $session->put('success', 'You\'ve marked '.$func->format($row['artist']).': '.$func->format($row['track']).' as viewed');
                            } else {
                                $session->put('error', 'That vid has already been viewed');
                            }
                        } else {
                            $session->put('error', 'That vid has already been cleared');
                        }
                    } else {
                        $session->put('error', 'The listing you selected doesn\'t exist');
                    }
                } else {
                    $session->put('error', 'You didn\'t select a valid listing');
                }
            } else {
                $session->put('error', 'You don\'t have access');
            }
            header('Location: /queue');

            return;
        } elseif ('clear' == $arg) {
            if ($func->perm('can_clear_queue')) {
                $db->query('SELECT COUNT(id) FROM track_queue WHERE played = 0 AND cleared = 0');
                $db->execute();
                if ($db->result() > 0) {
                    $db->query('UPDATE track_queue SET cleared = 1 WHERE played = 0 AND cleared = 0');
                    $db->execute();
                    $aff = $db->affected();
                    $session->put('success', 'The queue has been cleared');
                } else {
                    $session->put('error', 'There\'s no queue to clear');
                }
            } else {
                $session->put('error', 'You don\'t have access');
            }
            header('Location: /queue');

            return;
        } elseif ('add' == $arg) {
            if ($func->perm('can_add_to_queue')) {
                $func->req_file(VIEW_PATH.'queue-add.php');

                return;
            } else {
                $session->put('error', 'You don\'t have access');
                header('Location: /queue');

                return;
            }
        } elseif ('delete' == $arg) {
            if ($func->perm('can_delete_from_queue')) {
                $arg2 = ctype_digit($arg2) && $arg2 > 0 ? $arg2 : null;
                if (null !== $arg2) {
                    $db->query('SELECT id, track, artist FROM track_queue WHERE id = ?');
                    $db->execute([$arg2]);
                    $row = $db->fetch(true);
                    if (null !== $row) {
                        $db->query('UPDATE track_queue SET cleared = 1 WHERE id = ?');
                        $db->execute([$arg2]);
                        $session->put('success', 'Requested deleted');
                    } else {
                        $session->put('error', 'That queue listing doesn\'t exist');
                    }
                } else {
                    $session->put('error', 'You didn\'t supply a queue ID');
                }
            } else {
                $session->put('error', 'You don\'t have access');
            }
            header('Location: /queue');

            return;
        } elseif ('watch' == $arg) {
            $arg2 = ctype_digit($arg2) && $arg2 > 0 ? $arg2 : null;
            if (null !== $arg2) {
                global $row, $data;
                $db->query('SELECT id, played, calculated_hash, singer, is_solo FROM track_queue WHERE id = ?');
                $db->execute([$arg2]);
                $row = $db->fetch(true);
                if (null !== $row) {
                    if (!$row['played']) {
                        $db->query('UPDATE track_queue SET played = 1 WHERE id = ?');
                        $db->execute([$arg2]);
                    }
                    $data = $func->getSinger($row['singer']);
                    $func->req_file(VIEW_PATH.'queue-watch.php');
                    // header('Location: https://youtube.com/watch?v='.$row['calculated_hash']);

                    return;
                } else {
                    $session->put('error', 'That queue listing doesn\'t exist');
                }
            } else {
                $session->put('error', 'You didn\'t select a valid queue listing');
            }
            header('Location: /queue');

            return;
        } else {
            global $played, $queue;
            $welcome = $session->get('welcome');
            if (null !== $welcome) {
                $func->alert((strtotime($my['time_last_action']) > strtotime($my['time_added']) ? 'Welcome back' : 'Welcome').', '.$func->format($my['username']), 'info');
                $session->put('welcome', null);
            }
            $played = [];
            $db->query('SELECT COUNT(id) AS cnt, calculated_hash FROM track_queue GROUP BY calculated_hash');
            $db->execute();
            $stats = $db->fetch();
            if (null !== $stats) {
                foreach ($stats as $stat) {
                    $played[$stat['calculated_hash']] = $stat['cnt'];
                }
            }
            $db->query('SELECT * FROM track_queue WHERE played = 0 AND cleared = 0 ORDER BY time_added ASC');
            $db->execute();
            $queue = $db->fetch();
            $func->req_file(VIEW_PATH.'queue.php');

            return;
        }
    }

    public function post($arg = null)
    {
        global $db, $func, $my, $session;
        $my = $func->getUser();
        $extra = '';
        if (null === $my) {
            $session->put('error', 'You don\'t have access');
            header('Location: /queue');

            return;
        }
        if ('add' == $arg) {
            if ($func->perm('can_add_to_queue')) {
                $strs = ['track', 'artist', 'singer'];
                $_POST['track'] = array_key_exists('track', $_POST) && is_string($_POST['track']) && strlen($_POST['track']) > 0 ? strip_tags(trim($_POST['track'])) : null;
                $_POST['artist'] = array_key_exists('artist', $_POST) && is_string($_POST['artist']) && strlen($_POST['artist']) > 0 ? strip_tags(trim($_POST['artist'])) : null;
                $_POST['singer'] = array_key_exists('singer', $_POST) && is_string($_POST['singer']) && strlen($_POST['singer']) > 0 ? strip_tags(trim($_POST['singer'])) : 'All';
                $_POST['requester'] = array_key_exists('requester', $_POST) && is_string($_POST['requester']) && strlen($_POST['requester']) > 0 ? strip_tags(trim($_POST['requester'])) : '';
                $_POST['is_solo'] = array_key_exists('is_solo', $_POST) && isset($_POST['is_solo']) ? 1 : 0;
                if (null !== $_POST['track']) {
                    if (null !== $_POST['artist']) {
                        // Dupe check
                        $db->query('SELECT COUNT(id) FROM track_queue WHERE LOWER(artist) = ? AND LOWER(track) = ? AND played = 0');
                        $db->execute([strtolower($_POST['artist']), strtolower($_POST['track'])]);
                        if (!$db->result()) {
                            $apikey = 'AIzaSyD8pfizRjSnIvGmdE3Jatv1b8Pu684jrRU';
                            $googleApiUrl = 'https://www.googleapis.com/youtube/v3/search?part=snippet&q='.urlencode($_POST['track'].' '.$_POST['artist'].' "karaoke"').'&maxResults=1&key='.$apikey;
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_HEADER, 0);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch, CURLOPT_URL, $googleApiUrl);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                            curl_setopt($ch, CURLOPT_VERBOSE, 0);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            $response = curl_exec($ch);
                            curl_close($ch);
                            $data = json_decode($response);
                            $value = json_decode(json_encode($data), true);
                            $videoID = $value['items'][0]['id']['videoId'];
                            if (null != $videoID) {
                                $db->query('INSERT INTO track_queue (track, artist, singer, calculated_hash, added_by, requested_by, is_solo) VALUES (?, ?, ?, ?, ?, ?, ?)');
                                $db->execute([$_POST['track'], $_POST['artist'], $_POST['singer'], $videoID, $my['id'], $_POST['requester'], $_POST['is_solo']]);
                                $extra = array_key_exists('another', $_POST) && isset($_POST['another']) ? '/add' : '';
                                $session->put('success', 'Added to the queue');
                            } else {
                                $session->put('error', 'No results found');
                            }
                        } else {
                            $session->put('error', 'That track is already in the queue');
                        }
                    } else {
                        $session->put('error', 'You didn\'t enter a valid artist');
                    }
                } else {
                    $session->put('error', 'You didn\'t enter a valid track');
                }
            } else {
                $session->put('error', 'You don\'t have access');
            }
        } else {
            $session->put('error', 'wut?');
        }
        header('Location: /queue'.$extra);

        return;
    }
}
