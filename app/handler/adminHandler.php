<?php

if (!defined('SITE_ENABLE')) {
    exit;
}
class adminHandler
{
    public function get($arg = null, $arg2 = null, $arg3 = null)
    {
        global $db, $func, $my, $session;
        if (null === $my or false === $func->perm('can_access_admin_panel')) {
            $session->put('error', 'You don\'t have access');
            header('Location: /queue');

            return;
        }
        if ('roles' == $arg) {
            global $fields, $rows, $roles;
            $arg3 = ctype_digit($arg3) && $arg3 > 0 ? $arg3 : null;
            if (!$func->perm('can_manage_roles')) {
                $session->put('error', 'You don\'t have access');
                header('Location: /admin');

                return;
            }
            $fields = [];
            $db->query('SHOW COLUMNS FROM roles WHERE Type = "tinyint(4)"');
            $db->execute();
            $rows = $db->fetch();
            if (null === $rows) {
                $db->query('INSERT INTO roles (name, override_all) VALUES ("Administrator", 1)');
                $db->execute();
                header('Location: /admin/roles');

                return;
            }
            foreach ($rows as $row) {
                $fields[] = $row['Field'];
            }
            sort($fields, SORT_NATURAL);
            if ('view' == $arg2) {
                if (null !== $arg3) {
                    global $row;
                    $db->query('SELECT SQL_CACHE id, name, override_all FROM roles WHERE id = ?');
                    $db->execute([$arg3]);
                    $row = $db->fetch(true);
                    if (null !== $row) {
                        $func->req_file(VIEW_PATH.'admin/index.php');
                        $func->req_file(VIEW_PATH.'admin/roles/view.php');

                        return;
                    } else {
                        $session->put('error', 'The rank you selected doesn\'t exist');
                    }
                } else {
                    $session->put('error', 'You didn\'t select a valid role');
                }
                header('Location: /admin/roles');

                return;
            } else {
                global $cnts;
                $cnts = [];
                $db->query('SELECT role_id, COUNT(id) AS cnt FROM users WHERE role_id > 0 GROUP BY role_id');
                $db->execute();
                $memberGroups = $db->fetch();
                if (null !== $memberGroups) {
                    foreach ($memberGroups as $group) {
                        $cnts[$group['role_id']] = $group['cnt'];
                    }
                }
                $db->query('SELECT SQL_CACHE id, name FROM roles ORDER BY name ASC');
                $db->execute();
                $roles = $db->fetch();
                $func->req_file(VIEW_PATH.'admin/index.php');
                $func->req_file(VIEW_PATH.'admin/roles/index.php');

                return;
            }
        } elseif ('users' == $arg) {
            $func->req_file(VIEW_PATH.'admin/index.php');
            $func->alert('Coming soon', 'info');

            return;
        } elseif ('settings' == $arg) {
            if ($func->perm('can_manage_site_settings')) {
                $func->req_file(VIEW_PATH.'admin/index.php');
                $func->req_file(VIEW_PATH.'admin/settings.php');

                return;
            } else {
                $session->put('error', 'You don\'t have access');
            }
            header('Location: /admin');

            return;
        } else {
            $func->req_file(VIEW_PATH.'admin/index.php');

            return;
        }
    }

    public function post($arg = null, $arg2 = null, $arg3 = null)
    {
        global $db, $func, $my, $session;
        if (null === $my or false === $func->perm('can_access_admin_panel')) {
            $session->put('error', 'You don\'t have access');
            header('Location: /queue');

            return;
        }

        if ('roles' == $arg) {
            $fields = [];
            $db->query('SHOW COLUMNS FROM roles WHERE Type = "tinyint(4)"');
            $db->execute();
            $rows = $db->fetch();
            if (null === $rows) {
                $db->query('INSERT INTO roles (name, override_all) VALUES ("Administrator", 1)');
                $db->execute();
                header('Location: /admin/roles');

                return;
            }
            foreach ($rows as $row) {
                $fields[] = $row['Field'];
            }
            sort($fields, SORT_NATURAL);
            if ($func->perm('can_manage_roles')) {
                $arg3 = ctype_digit($arg3) && $arg3 > 0 ? $arg3 : null;
                if ('add' == $arg2) {
                    unset($_POST['submit']);
                    $_POST['name'] = array_key_exists('name', $_POST) && is_string($_POST['name']) && strlen($_POST['name']) > 0 ? trim(strip_tags($_POST['name'])) : null;
                    if (null !== $_POST['name']) {
                        $db->query('SELECT SQL_CACHE COUNT(id) FROM roles WHERE name = ?');
                        $db->execute([$_POST['name']]);
                        if (!$db->result()) {
                            $query = '';
                            $params = [];
                            foreach ($_POST as $what => $value) {
                                $query .= ', '.$what.' = ?';
                                $params[] = $value;
                            }
                            $db->trans('start');
                            $db->query('INSERT INTO roles (name) VALUES (?)');
                            $db->execute([$_POST['name']]);
                            $new = $db->insert_id();
                            if ('' != $query) {
                                $params[] = $new;
                                $query = substr($query, 2);
                                $db->query('UPDATE roles SET '.$query.' WHERE id = ?');
                                $db->execute($params);
                            }
                            $db->trans('end');
                            $session->put('success', "You've added the ".$func->format($_POST['name']).' role');
                        } else {
                            $session->put('error', 'Another rank with that name already exists');
                        }
                    } else {
                        $session->put('error', 'You didn\'t enter a valid name');
                    }
                } elseif ('edit' == $arg2) {
                    if (null !== $arg3) {
                        $db->query('SELECT SQL_CACHE id, name, override_all FROM roles WHERE id = ?');
                        $db->execute([$arg3]);
                        $row = $db->fetch(true);
                        if (null !== $row) {
                            unset($_POST['submit'], $_POST['pull']);
                            $db->trans('start');
                            foreach ($fields as $what) {
                                $value = array_key_exists($what, $_POST) && isset($_POST[$what]) ? 1 : 0;
                                $db->query('UPDATE roles SET '.$what.' = ? WHERE id = ?');
                                $db->execute([$value, $arg3]);
                            }
                            $db->trans('end');
                            $session->put('success', 'You\'ve edited the staff rank: '.$func->format($row['name']));
                        } else {
                            $session->put('error', 'The rank you selected doesn\'t exit');
                        }
                    } else {
                        $session->put('error', 'You didn\'t select a valid role');
                    }
                } elseif ('delete' == $arg2) {
                    if (null !== $arg3) {
                        $db->query('SELECT SQL_CACHE id, name FROM roles WHERE id = ?');
                        $db->execute([$arg3]);
                        $rank = $func->format($db->result(1));
                        if (null !== $rank) {
                            if (1 != $arg3) {
                                $db->trans('start');
                                $db->query('UPDATE users SET role_id = 0 WHERE role_id = ?');
                                $db->execute([$arg3]);
                                $db->query('DELETE FROM roles WHERE id = ?');
                                $db->execute([$arg3]);
                                $db->trans('end');
                                $session->put('success', 'You\'ve deleted the '.$func->format($rank['name']).' role');
                            } else {
                                $session->put('error', 'You can\'t delete the Administrator role');
                            }
                        } else {
                            $session->put('error', 'The role you selected doesn\'t exist');
                        }
                    } else {
                        $session->put('error', 'You didn\'t select a valid role');
                    }
                } else {
                    $session->put('error', 'wut?');
                }
            } else {
                $session->put('error', 'You don\'t have access');
            }
        } elseif ('settings' == $arg) {
            if ($func->perm('can_manage_site_settings')) {
                $switches = ['enable_registration', 'taking_requests', 'room_open'];
                $db->trans('start');
                $db->query('UPDATE settings SET conf_value = ? WHERE conf_name = ?');
                foreach ($switches as $what) {
                    $_POST[$what] = array_key_exists($what, $_POST) && isset($_POST[$what]) ? 1 : 0;
                    $db->execute([$_POST[$what], $what]);
                }
                $db->trans('end');
                $session->put('success', 'Settings updated');
            } else {
                $session->put('error', 'You don\'t have access');
            }
        } else {
            $session->put('error', 'wut?');
        }
        header('Location: /admin');

        return;
    }
}
