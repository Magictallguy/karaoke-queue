<?php

if (!defined('SITE_ENABLE')) {
    exit;
}
class authHandler
{
    public function get($arg = null)
    {
        global $db, $func, $my, $set, $session;
        if ('logout' == $arg) {
            global $session;
            $session->stop();
            header('Location: /queue');

            return;
        } elseif ('login' == $arg) {
            if (null !== $my) {
                header('Location: /queue');

                return;
            }
            $func->req_file(VIEW_PATH.'login.php');

            return;
        } elseif ('reg' == $arg) {
            if (null === $my) {
                if ($set['enable_registration']) {
                    $func->req_file(VIEW_PATH.'reg.php');

                    return;
                } else {
                    $session->put('error', 'Registration has been disabled');
                }
            } else {
                $session->put('error', 'You\'re already logged in');
            }
            header('Location: /queue');

            return;
        }
    }

    public function post($arg = null)
    {
        global $db, $func, $my, $set, $session;
        if ('login' == $arg) {
            if (null !== $my) {
                header('Location: /queue');

                return;
            }
            $_POST['username'] = array_key_exists('username', $_POST) && is_string($_POST['username']) && strlen($_POST['username']) > 0 ? strip_tags(trim($_POST['username'])) : null;
            $_POST['password'] = array_key_exists('password', $_POST) && is_string($_POST['password']) && strlen($_POST['password']) > 0 ? $_POST['password'] : null;
            if (null !== $_POST['username']) {
                if (null !== $_POST['password']) {
                    $db->query('SELECT id, username, password FROM users WHERE LOWER(username) = ?');
                    $db->execute([strtolower($_POST['username'])]);
                    $user = $db->fetch(true);
                    if (null !== $user) {
                        if (password_verify($_POST['password'], $user['password'])) {
                            $session->put('userid', $user['id']);
                            $session->put('welcome', true);
                        } else {
                            $session->put('error', 'Invalid username/password combo');
                        }
                    } else {
                        $session->put('error', 'User not found');
                    }
                } else {
                    $session->put('error', 'You didn\'t enter a password');
                }
            } else {
                $session->put('error', 'You didn\'t enter a username');
            }
            header('Location: /queue');

            return;
        } elseif ('req' == $arg) {
            if (null !== $my) {
                header('Location: /queue');

                return;
            }
            if (!$set['enable_registration']) {
                $session->put('error', 'Registration has been disabled');
                header('Location: /queue');

                return;
            }
            $_POST['username'] = array_key_exists('username', $_POST) && is_string($_POST['username']) && strlen($_POST['username']) > 0 ? strip_tags(trim($_POST['username'])) : null;
            $_POST['password'] = array_key_exists('password', $_POST) && is_string($_POST['password']) && strlen($_POST['password']) > 0 ? $_POST['password'] : null;
            $_POST['conf_password'] = array_key_exists('conf_password', $_POST) && is_string($_POST['conf_password']) && strlen($_POST['conf_password']) > 0 ? $_POST['conf_password'] : null;
            if (null !== $_POST['username'] && null !== $_POST['password'] && null !== $_POST['conf_password']) {
                if ($_POST['password'] === $_POST['conf_password']) {
                    $db->query('SELECT COUNT(id) FROM users WHERE LOWER(username) = ?');
                    $db->execute([strtolower($_POST['username'])]);
                    if (!$db->result()) {
                        $db->query('INSERT INTO users (username, password) VALUES (?, ?)');
                        $db->execute([$_POST['username'], password_hash($_POST['password'], PASSWORD_BCRYPT)]);
                        $id = $db->insert_id();
                        if ($id > 0) {
                            $session->put('userid', $id);
                            $session->put('welcome', true);
                        } else {
                            $session->put('error', 'Couldn\'t create the account');
                        }
                    } else {
                        $session->put('error', 'That username is already in use');
                    }
                } else {
                    $session->put('error', 'The passwords you entered didn\'t match');
                }
            }
            header('Location: /queue');

            return;
        }
    }
}
