<?php
if (!defined('SITE_ENABLE')) {
    exit;
}
global $func, $pages, $my, $queue, $played, $session, $set;
if (!$set['room_open']) {
    $func->alert('Bear\'s room is currently closed', 'info');
} else {
    if ($set['taking_requests']) {
        $func->alert('Taking requests!', 'info');
    }
}
$cnt = 0;
$keys = ['error', 'warning', 'info', 'success'];
foreach ($keys as $key) {
    ${$var} = $session->get($key);
    if (null !== ${$var}) {
        ?>
        <div class="alert alert-<?php echo 'error' != $key ? $key : 'danger'; ?>">
            <strong><?php echo ucfirst($key); ?></strong><br>
            <?php echo ${$var}; ?>
        </div><?php
        $session->put($key, null);
    }
} ?>
<h3 class="mb-4">The Queue</h3><?php
if ($func->perm(['can_add_to_queue', 'can_clear_queue'])) {
    ?>
    <p><?php
if ($func->perm('can_add_to_queue')) {
        ?>
        <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#queue-add">
            <span class="fas fa-plus"></span>
            <span class="sr-only">Add</span>
        </button><?php
    }
    if ($func->perm('can_clear_queue')) {
        ?>
        <a href="/queue/clear" class="btn btn-sm btn-danger">
            <span class="fas fa-trash"></span>
            <span class="sr-only">Clear</span>
        </a><?php
    } ?>
</p><?php
} ?>
<p>
    <table class="table bg-dark text-light">
        <thead>
            <tr>
                <th>Artist: Track</th>
                <th>Played</th>
                <th>Added</th>
                <th>Requester</th><?php
if (null !== $my) {
        ?>
                <th>Adder</th>
                <th>Actions</th><?php
    } ?>
            </tr>
        </thead>
        <tbody><?php
        if (null === $queue) {
            ?>
            <tr>
                <td colspan="<?php echo null !== $my ? 6 : 4; ?>" class="text-center">
                    There's nothing in the queue<br>
                    Post your requests in the chat!
                </td>
            </tr><?php
        } else {
            $hasPerm = $func->perm('can_mark_queue');
            foreach ($queue as $row) {
                ++$cnt;
                $date = new \DateTime($row['time_added']); ?>
            <tr<?php echo 1 == $cnt ? ' style="background-color:#384639;"' : ''; ?>>
                <td>
                    <a href="/queue/watch/<?php echo $row['id']; ?>" class="text-warning" target="new"><?php echo ucwords($func->format($row['artist'])); ?>: <?php echo ucwords($func->format($row['track'])); ?></a><br>
                    <em>(<?php echo $row['singer'] ? $func->format($row['singer']) : 'All'; ?>)</em>
                </td>
                <td><?php echo array_key_exists($row['calculated_hash'], $played) ? $func->humanize($played[$row['calculated_hash']]) : 'Never'; ?></td>
                <td><?php echo $date->format('H:i:s').'<br>'.$date->format('d/m/Y'); ?></td>
                <td><?php echo '' != $row['requested_by'] ? $func->format($row['requested_by']) : '<em>No-one</em>'; ?></td><?php
                if (null !== $my) {
                    ?>
                    <td><?php echo $func->username($row['added_by']); ?></td>
                    <td><?php
                    if (true === $hasPerm) {
                        ?>
                        <a href="/queue/viewed/<?php echo $row['id']; ?>" class="btn btn-warning" title="Mark as viewed">
                            <span class="fas fa-eye"></span>
                            <span class="sr-only">Mark as viewed</span>
                        </a><?php
                    } ?>
                        <a href="/queue/delete/<?php echo $row['id']; ?>" class="btn btn-danger" title="Delete">
                            <span class="fas fa-trash"></span>
                            <span class="sr-only">Delete</span>
                        </a>
                    </td><?php
                } ?>
            </tr><?php
            }
        } ?>
        </tbody>
    </table>
</p>
<?php
if ($func->perm('can_add_to_queue')) {
            ?>
<!-- Modal -->
<div class="modal fade" id="queue-add" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content bg-dark text-light">
      <div class="modal-header">
        <h5 class="modal-title" id="ModalLabel">Add to queue</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form action="/queue/add" method="post">
              <div class="form-row">
                  <div class="col-6">
                      <div class="form-group">
                          <label for="artist">Artist</label>
                          <input type="text" name="artist" id="artist" class="form-control" required autofocus>
                      </div>
                  </div>
                  <div class="col-6">
                      <div class="form-group">
                          <label for="track">Track</label>
                          <input type="text" name="track" id="track" class="form-control" required>
                      </div>
                  </div>
              </div>
              <div class="form-row">
                  <div class="col-6">
                      <div class="form-group">
                          <label for="singer">Singer</label>
                          <input type="text" name="singer" id="singer" class="form-control" placeholder="Leave blank for &ldquo;All&rdquo;">
                      </div>
                  </div>
                  <div class="col-6">
                      <div class="form-group">
                          <label for="requester">Requester</label>
                          <input type="text" name="requester" id="requester" class="form-control" placeholder="Optional - for reference">
                      </div>
                  </div>
              </div>
              <div class="form-row">
                  <div class="form-controls">
                      <div class="form-check">
                          <input type="checkbox" name="is_solo" id="is_solo" value="1" class="form-check-input">
                          <label for="is_solo" class="form-check-label">Solo?</label>
                      </div>
                      <button type="submit" name="submit" class="btn btn-primary">Add to queue</button>
                  </div>
              </div>
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div><?php
        }
