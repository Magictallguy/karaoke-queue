<?php
if (!defined('SITE_ENABLE')) {
    exit;
}
global $func, $row, $data; ?>
<h3 class="mb-4"><?php echo $data['singer']; ?></h3>
<p class="text-center">
    <iframe
        width="956"
        height="538"
        src="https://www.youtube.com/embed/<?php echo $row['calculated_hash']; ?>"
        frameborder="0"
        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
        allowfullscreen>
    </iframe>
</p>
