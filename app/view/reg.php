<?php
if (!defined('SITE_ENABLE')) {
    exit;
} ?>
<h3 class="mb-4">Register</h3>
<p>
    <form action="/auth/reg" method="post">
        <div class="form-group">
            <label for="username">Username</label>
            <input type="text" name="username" id="username" class="form-control" required autofocus>
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" name="password" id="password" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="conf_password">Confirm Password</label>
            <input type="password" name="conf_password" id="conf_password" class="form-control" required>
        </div>
        <div class="form-controls">
            <button type="submit" class="btn btn-primary">Register</button>
        </div>
    </form>
</p>
