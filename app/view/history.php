<?php
if (!defined('SITE_ENABLE')) {
    exit;
}
global $func, $my, $pages, $history, $played, $canRestore, $replayed;
$songCnt = count($played); ?>
<h3 class="mb-4">History</h3><?php
echo $func->format($songCnt); ?> song<?php echo $func->s($songCnt); ?> played &middot; <?php echo $func->format($replayed); ?> repeat<?php echo $func->s($replayed).'<br><br>';
if (null !== $my && $func->perm('can_clear_history')) {
    ?>
    <a href="/history/clear" class="btn btn-danger" title="Clear history">
        <span class="fas fa-trash"></span>
        <span class="sr-only">Clear</span>
    </a><?php
}
if (null === $history) {
    ?>
<p>
    There's nothing in the history
</p><?php
} else {
        ?>
<div class="float-right">
    <?php echo $pages->display_pages(); ?>
</div>
<p>
    <table class="table bg-dark text-light">
        <thead>
            <tr>
                <th>Artist: Track</th>
                <th>Singer(s)</th>
                <th>Added</th>
                <th>Played</th><?php
if (null !== $my) {
            ?>
                <th>Requester</th>
                <th>Adder</th><?php
    if (true === $canRestore) {
        ?>
                <th>Actions</th><?php
    }
        } ?>
            </tr>
        </thead>
        <tbody><?php
    if (null === $history) {
        ?>
            <tr>
                <td colspan="<?php echo null !== $my ? 6 : 4; ?>" class="text-center">There is no history</td>
            </tr><?php
    } else {
        foreach ($history as $row) {
            $date = new DateTime($row['time_added']); ?>
            <tr>
                <td><?php echo ucwords($func->format($row['artist'])).': '.ucwords($func->format($row['track'])); ?></td>
                <td><?php echo $row['singer'] ? $func->format($row['singer']) : 'All'; ?></td>
                <td><?php echo $date->format('H:i:s').'<br>'.$date->format('d/m/Y'); ?></td>
                <td><?php echo array_key_exists($row['calculated_hash'], $played) ? $func->humanize($played[$row['calculated_hash']]) : 'Never'; ?></td><?php
        if (null !== $my) {
            ?>
                <td><?php echo $func->format($row['requested_by']); ?></td>
                <td><?php echo $func->username($row['added_by']); ?></td><?php
            if (true === $canRestore) {
                ?>
                <td>
                    <a href="/history/restore/<?php echo $row['id']; ?>" class="btn btn-primary" title="Restore to queue">
                        <span class="fas fa-recycle"></span>
                        <span class="sr-only">Restore</span>
                    </a>
                </td><?php
            }
        } ?>
            </tr><?php
        }
    } ?>
        </tbody>
    </table>
</p>
<div class="float-right">
    <?php echo $pages->display_pages(); ?>
</div><?php
    }
