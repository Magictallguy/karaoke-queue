<?php
if (!defined('SITE_ENABLE')) {
    exit;
} ?>
<h3 class="mb-4">Add to Queue</h3>
<p>
    <form action="/queue/add" method="post">
        <div class="form-row">
            <div class="col-6">
                <div class="form-group">
                    <label for="artist">Artist</label>
                    <input type="text" name="artist" id="artist" class="form-control" required autofocus>
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label for="track">Track</label>
                    <input type="text" name="track" id="track" class="form-control" required>
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="col-6">
                <div class="form-group">
                    <label for="singer">Singer (leave blank for all)</label>
                    <input type="text" name="singer" id="singer" class="form-control">
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label for="requester">Requester (optional - for reference)</label>
                    <input type="text" name="requester" id="requester" class="form-control">
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="col">
                <div class="form-check">
                    <input type="checkbox" name="is_solo" id="is_solo" value="1" class="form-check-input">
                    <label for="is_solo" class="form-check-label">Solo?</label>
                </div>
                <div class="form-check">
                    <input type="checkbox" name="another" id="another" value="1" class="form-check-input" checked>
                    <label for="another" class="form-check-label">Add another</label>
                </div>
                <div class="form-controls">
                    <button type="submit" name="submit" class="btn btn-primary">Add to queue</button>
                </div>
            </div>
        </div>
    </form>
</p>
