<?php
if (!defined('SITE_ENABLE')) {
    exit;
} ?>
<style scoped>
ul>li>ul>li {
    line-height: 2em;
}
</style>
<h3 class="mb-4">FAQ</h3>
<ul style="list-style:none;">
    <li>Who wrote this site?<ul>
        <li><a href="https://rabb.it/Orsokuma1" target="new">@Orsokuma1</a></li>
    </ul></li>
    <li>Why?<ul>
        <li>The intention was to make <a href="https://rabb.it/JimmyJack" target="new">@JimmyJack</a>'s life a little easier</li>
    </ul></li>
    <li>Why does Rabbit not connect me to everyone else?<ul>
        <li>If you're using Chrome, then one of Rabbit's updates conflicts with Chrome's WebRTC. Easiest way around? Use Firefox</li>
    </ul></li>
</ul>
