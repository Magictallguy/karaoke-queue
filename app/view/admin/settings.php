<?php
if (!defined('SITE_ENABLE')) {
    exit;
}
global $set; ?>
<h3 class="mb-4">Site Settings</h3>
<p>
    <form action="/admin/settings" method="post">
        <div class="form-check">
            <input type="checkbox" name="enable_registration" id="enable_registration" value="1" class="form-check-input"<?php echo $set['enable_registration'] ? ' checked' : ''; ?>>
            <label for="enable_registration">Enable Registration</label>
        </div>
        <div class="form-check">
            <input type="checkbox" name="taking_requests" id="taking_requests" value="1" class="form-check-input"<?php echo $set['taking_requests'] ? ' checked' : ''; ?>>
            <label for="taking_requests">Taking requests</label>
        </div>
        <div class="form-check">
            <input type="checkbox" name="room_open" id="room_open" value="1" class="form-check-input"<?php echo $set['room_open'] ? ' checked' : ''; ?>>
            <label for="room_open">Room open?</label>
        </div>
        <div class="form-controls">
            <button type="submit" class="btn btn-primary">
                <span class="fas fa-check"></span>
                Update settings
            </button>
        </div>
    </form>
</p>
