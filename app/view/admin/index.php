<?php
if (!defined('SITE_ENABLE')) {
    exit;
} ?>
<h3 class="mb-4">Administration Panel</h3>
<ul class="nav">
    <li class="nav-item">
        <a href="/admin/settings" class="nav-link text-info">Site Settings</a>
    </li>
    <li class="nav-item">
        <a href="/admin/roles" class="nav-link text-info">Roles</a>
    </li>
    <li class="nav-item">
        <a href="/admin/users" class="nav-link text-info">Users</a>
    </li>
</ul>
