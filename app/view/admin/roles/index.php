<?php
if (!defined('SITE_ENABLE')) {
    exit;
}
// https://discordapp.com/api/webhooks/577670982764986370/fX1LFLvUlU2IpaiT7islVxeTpM57AhoQbrCCPJTr0XUNNVkuESAOCRe09zUk5jOv_iza
global $db, $func, $roles, $cnts, $fields; ?>
<h3 class="mb-4">Roles</h3>
<button type="button" href="/admin/roles/add" class="btn btn-success" title="Add" data-toggle="modal" data-target="#role-add">
    <span class="fa fa-plus"></span>
    <span class="sr-only">Add</span>
</button>
<p>
    <table class="table table-bordered table-dark table-hover table-striped w-100">
        <thead>
            <tr>
                <th>Rank</th>
                <th>Count</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody><?php
if (null === $roles) {
    ?>
            <tr>
                <td colspan="2" class="centre">There are no access roles</td>
            </tr><?php
} else {
        foreach ($roles as $row) {
            ?>
            <tr>
                <td><?php echo $func->format($row['name']); ?></td>
                <td><?php echo array_key_exists($row['id'], $cnts) && $cnts[$row['id']] > 0 ? $func->format($cnts[$row['id']]).' member'.$func->s($cnts[$row['id']]) : 'No-one'; ?></td>
                <td>
                    <a href="/admin/roles/view/<?php echo $row['id']; ?>" class="btn btn-primary" title="View/Edit">
                        <span class="fa fa-edit"></span>
                        <span class="sr-only">View/Edit</span>
                    </a>
                    <a href="/admin/roles/delete/<?php echo $row['id']; ?>" class="btn btn-danger" title="Delete">
                        <span class="fas fa-trash"></span>
                        <span class="sr-only">Delete</span>
                    </a>
                </td>
            </tr><?php
        }
    } ?>
        </tbody>
    </table>
</p>

<!-- Modal -->
<div class="modal fade" id="role-add" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content bg-dark text-light">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalLabel">Add role</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/admin/roles/add" method="post">
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" name="name" id="name" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="row row-odd"><?php
$co = 1;
foreach ($fields as $what) {
    ?>
                        <div class="col-3">
                            <?php echo ucwords(str_replace('_', ' ', $what)); ?>
                            <label class="switch" for="<?php echo $what; ?>">
                                <input type="checkbox" name="<?php echo $what; ?>" id="<?php echo $what; ?>" value="1" class="success">
                                <span class="slider round"></span>
                            </label>
                        </div><?php
if (0 == $co % 4 && 0 != $co) {
        ?>
                    </div>
                    <div class="row row-odd"><?php
    } elseif (0 == $co % 4) {
        ?>
                    <div class="row row-odd"><?php
    }
    ++$co;
}
$deficit = ceil($co % 4);
if (3 == $deficit) {
    $deficit = 1;
} elseif (1 == $deficit) {
    $deficit = 3;
}
if ($deficit > 0) {
    echo str_repeat('<div class="col-3">&nbsp;</div>', $deficit);
} ?>
                    </div>
                    <div class="row">
                        <div class="col-12 text-center">
                            <button type="submit" name="submit" class="btn btn-primary">Add Role</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
