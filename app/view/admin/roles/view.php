<?php
if (!defined('SITE_ENABLE')) {
    exit;
}
global $db, $func, $row, $fields; ?>
<h3 class="mb-4">View Role</h3>
<p>
    <table class="table table-bordered table-dark table-striped w-100" id="dataTable">
        <thead>
            <tr>
                <th colspan="2"><?php echo $func->format($row['name']); ?></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="2">
                    <form action="/admin/roles/edit/<?php echo $row['id']; ?>" method="post">
                        <div class="row row-odd"><?php
$co = 1;
foreach ($fields as $what) {
    $db->query('SELECT SQL_CACHE id, '.$what.' FROM roles WHERE id = ?');
    $db->execute([$row['id']]);
    $single = $db->result(1); ?>
                            <div class="col-3">
                                <?php echo ucwords(str_replace('_', ' ', $what)); ?>
                                <label class="switch" for="<?php echo $what; ?>">
                                    <input type="checkbox" name="<?php echo $what; ?>" id="<?php echo $what; ?>" value="1"<?php echo 1 == $single || 1 == $row['override_all'] ? ' checked' : ''; ?> class="success">
                                    <span class="slider round"></span>
                                </label>
                            </div><?php
    if (0 == $co % 4 && 0 != $co) {
        ?>
                        </div>
                        <div class="row row-odd"><?php
    } elseif (0 == $co % 4) {
        ?>
                        <div class="row row-odd"><?php
    }
    ++$co;
}
$deficit = ceil($co % 4);
if (3 == $deficit) {
    $deficit = 1;
} elseif (1 == $deficit) {
    $deficit = 3;
}
if ($deficit > 0) {
    echo str_repeat('<div class="col-3">&nbsp;</div>', $deficit);
} ?>
                        </div>
                        <div class="row">
                            <div class="col-12 text-center">
                                <button type="submit" name="submit" class="btn btn-primary">Update Role</button>
                            </div>
                        </div>
                    </form>
                </td>
            </tr>
        </tbody>
    </table>
</p>
