<?php
if (!defined('SITE_ENABLE')) {
    exit;
}
global $func, $rows, $requesterCnt, $attributeClass; ?>
<h3 class="mb-4">Top <?php echo $requesterCnt; ?> Requesters</h3>
<p>
    <table class="table bg-dark text-light">
        <thead>
            <tr>
                <th>Requester</th>
                <th>Count</th>
            </tr>
        </thead>
        <tbody><?php
if (null === $rows) {
    ?>
            <tr>
                <td colspan="2" class="text-center">No-one! wut</td>
            </tr><?php
} else {
        $cnt = 0;
        foreach ($rows as $row) {
            ++$cnt; ?>
            <tr>
                <td><span class="<?php echo array_key_exists($cnt, $attributeClass) ? $attributeClass[$cnt] : 'text-muted'; ?>"><?php echo $func->ordinal($cnt); ?></span> &middot; <?php echo $func->format($row['requested_by']); ?></td>
                <td><?php echo $func->format($row['cnt']); ?></td>
            </tr><?php
        }
    } ?>
        </tbody>
    </table>
</p>
